'use strict'

angular.module('xmasApp.users', [
    'ngRoute',
    'firebase'
])

    .config(['$routeProvider', function($routeProvider){
      $routeProvider.when('/users', {
        templateUrl: 'xmasApp/users.html',
        controller: 'UsersCtrl'
      });
    }])

    .controller('UsersCtrl', ['$scope', '$firebaseArray', function($scope, $firebaseArray){

      $scope.showUserDetails = false;

      // Init Firebase
      var ref = new Firebase('https://mycontacts-my-test-app.firebaseio.com/users');

      // Get Users
      $scope.users = $firebaseArray(ref);

      // Show Add Name and Wish Form
      $scope.showAddNameAndWish = function(){
        $scope.addNameAndWishFormShow = true;
      };

      // Close Add Name and Wish Form
      $scope.hideAddNameAndWish = function(){
        $scope.addNameAndWishFormShow = false;
      };

      // Submit Add Name and Wish Form
      $scope.addNameAndWish = function(){
        console.log('Ad name and widh');
        $scope.addNameAndWishFormShow = false

        // Assign Values
        if($scope.name) {
          var name = $scope.name;
        } else {
          name = null;
        }
        if($scope.text) {
          var text = $scope.text;
        } else {
          text = null;
        }
        if($scope.misto) {
          var misto = $scope.misto;
        } else {
          misto = null;
        }
        if($scope.url) {
          var url = $scope.url;
        } else {
          url = null;
        }

        // Build Object
        $scope.users.$add({
          name: name,
          text: text,
          misto: misto,
          url: url
        });

        // Then Clear Form
        clearFields();

        // Then Send Message
      };

      // Show Name and Wish

      // Clear Form Function
      function clearFields() {
        $scope.name = '';
        $scope.text = '';
        $scope.url = '';
      }

    }]);