'use strict';

// Declare app level module which depends on views, and components
angular.module('myContacts', [
  'ngRoute',
  'firebase',
  'myContacts.contacts',
  'xmasApp.users'
])
    .config(['$routeProvider', function($routeProvider) {
      $routeProvider.otherwise({redirectTo: '/users'});
    }])

    .controller('MainCtrl', ['$scope', function($scope){
      $scope.heading = "Heading";
    }]);