'use strict';

angular.module('myContacts.contacts', [
  'ngRoute',
  'firebase'
])
    .config(['$routeProvider', function($routeProvider) {
      $routeProvider.when('/contacts', {
        templateUrl: 'contacts/contacts.html',
        controller: 'ContactsCtrl'
      });
    }])
    .controller('ContactsCtrl', ['$scope', '$firebaseArray', function($scope, $firebaseArray) {
      // Init Firebase
      var ref = new Firebase("https://mycontacts-my-test-app.firebaseio.com/contacts");

      // Get Contacts
      $scope.contacts = $firebaseArray(ref);

      // Show Add Form
      $scope.showAddForm = function() {
        $scope.addFormShow = true;
      };

      // Show Edit Form
      $scope.showEditForm = function(contact){
        $scope.editFormShow = true;

        $scope.id           = contact.$id;
        $scope.name         = contact.name;
        $scope.email        = contact.email;
        $scope.home_phone   = contact.phone[0].home;
        $scope.mobile_phone = contact.phone[0].mobile;
      };

      $scope.closeAddForm = function() {
        $scope.addFormShow = false;
        $scope.contactShow = false;
        $scope.editFormShow = false;
      };



      $scope.addFormSubmit = function() {

        // Assign Values
        if($scope.name){var name = $scope.name} else {name = null};
        if($scope.email){var email = $scope.email} else {email = null};
        if($scope.home_phone){var home = $scope.home_phone} else{home = null};
        if($scope.mobile_phone){var mobile = $scope.mobile_phone} else{mobile = null};

        // Build Object
        $scope.contacts.$add({
          name: name,
          email: email,
          phone: [
            {
              home: home,
              mobile: mobile
            }
          ]
        }).then(function(ref){
          // get key
          var id = ref.key();
          console.log('Added contact with ID: '+id);

          // clear form
          clearFields();

          // hide form
          $scope.addFormShow = false;

          // send message
          $scope.msg = "Contact added.";
        });
      };

      $scope.editFormSubmit = function(){
        console.log('Edit form...');

        // Get ID
        var id = $scope.id;

        // Get Record
        var record = $scope.contacts.$getRecord(id);

        // Assign Values
        record.name            = $scope.name;
        record.email           = $scope.email;
        record.phone[0].home   = $scope.mobile_phone;
        record.phone[0].mobile = $scope.home_phone;

        $scope.contacts.$save(record).then(function(ref){
          console.log(ref.key);
        });

        // Clear all the fields
        clearFields();

        // Hide Form
        $scope.editFormShow = false;

        // Message
        $scope.msg = "Contact Updated"
      };

      $scope.showContact = function(contact) {
        $scope.name         = contact.name;
        $scope.email        = contact.email;
        $scope.mobile_phone = contact.phone[0].mobile;
        $scope.home_phone = contact.phone[0].home;

        $scope.contactShow = true;
      };

      $scope.removeContact = function(contact){
        $scope.contacts.$remove(contact);
        $scope.msg = "Contact Removed";
      }

      function clearFields() {
        $scope.name = '';
        $scope.email = '';
        $scope.home_phone = '';
        $scope.mobile_phone = '';
      };

    }]);